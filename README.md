# Journée du 11/04/2014

## Théorie du TDD

Introduction au TDD : voir slides [ici](http://fr.slideshare.net/xnopre/human-talks-grenoble-11122012-tdd?ref=http://xnopre.blogspot.fr/2012/12/human-talks-grenoble-du-11-dec-2012.html "Title")

## Mise en pratique

#### Technos utilisées

* Python : [docs](https://docs.python.org/release/2.6/)
* Framework de test : [nosetests](https://nose.readthedocs.org/en/latest/)


#### Exercice de la banque.

* En tant que personne, je veux pouvoir créer un compte
* En tant qu'utilisateur, je veux pouvoir consulter mon solde.
* En tant qu'utilisateur, je veux pouvoir déposer de l'argent
* En tant qu'utilisateur, je veux pouvoir retirer de l'argent
* En tant qu'utilisateur, je veux empecher de retirer de l'argent si solde inférieur à 200
* En tant qu'utilisateur, je veux pouvoir consulter l'historique de mes transactions ( avec une date et un montant )
* En tant qu'utilisateur, je veux pouvoir faire un virement.


#### trucs utiles

* lancer les tests: `nosetests -v -s -d NOM`
* Notion de `setUp`et `tearDown`: la fonction `setUp` va être éxécutée avant chaque test et la fonction `tearDown` après chaque test
* Capturer des exceptions avec `@raises(MonException)`
* Pour accéder au dernier élément d'une liste en python, on peut utiliser l'index `-1`. Par exemple si `l = [5, 4, 3, 6]` alors `l[-1] = 6`

# Journée du 16/04/2014

### Stories

* Changer le virement pour le mettre au niveau de la banque
* Un virement doit être entre deux comptes d'une même banque


### Notion de builder

Lorsque l'on suit la structure de test suivante : 

* Given
* When
* Then

Le *Given* nous permet de nous mettre dans un état donné.
Le *Then* permet d'appliquer une transition à cet état
Le *When* permet de tester le nouvel état.

Très souvent, on teste plusieurs transitions à partir d'un même état et pour éviter de copier/coller du code, on introduit la notion de **builder**.

Exemple : 

```
def account_builder_with_balance_equal_zero():
	person = Person()
	bank = Bank()
	bank.create_account(person)
	return bank.get_account(person)

def two_accounts_builder_with_balance_equal_zero(bank):	
	first_person = Person()
	second_person = Person()
	bank.create_account(first_person)
	bank.create_account(second_person)
	return bank.get_account(second_person), bank.get_account(first_person)

```

### Ce que l'on a appris

* Attention à ne pas se lancer dans des refactos massifs. Une notion à la fois.
* **Do not Repeat Yourself**
* Ne pas hésiter à enlever des classes inutiles ( Person )

# Journée du 30/04/14

### Stories

* En tant qu'utilisateur, je veux pouvoir mettre en place un virement permanent.
* En tant qu'utilisateur, je veux pouvoir imprimer un RIB.
* En tant que banque, je veux pouvoir donner des intérêts.
* En tant que personne, je veux connaitre tous mes comptes




