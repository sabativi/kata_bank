#!/usr/bin/env python
# -*- coding: utf-8 -*-
from nose.tools import istest, raises
from bank import Bank, NotPossibleWithDrawException, Regular_Transfer, TransferBetweenTwoBanksException, NotPossibleDepositException, History
from datetime import date

def account_builder_with_balance_equal_zero(*args):
	if(len(args)==0):
		bank = Bank("HSBC")
	else:
		assert isinstance(args[0],Bank)  
		bank = args[0]

	id_account = bank.create_account()
	return bank.get_account(id_account)

def two_accounts_builder_with_balance_equal_zero(bank):	
	id_account_1 = bank.create_account()
	id_account_2 = bank.create_account()
	return bank.get_account(id_account_1), bank.get_account(id_account_2)

@istest
def check_account_creation():
	# Given
	bank = Bank("HSBC")
	# When
	id_account = bank.create_account()
	# Then
	assert bank.account_exists(id_account)

@istest
def check_i_can_consult_the_balance_of_an_account():
	# Given
	account = account_builder_with_balance_equal_zero()
	# When
	balance = account.get_balance()
	# Then
	assert balance is not None

@istest
def check_I_can_deposit_money_on_my_account():
	#Given
	account = account_builder_with_balance_equal_zero()
	previous_balance = account.get_balance()
	amount_to_deposit = 255
	#When
	account.deposit(amount_to_deposit)
	#Then
	expected_balance = previous_balance + amount_to_deposit
	assert account.get_balance() == expected_balance

@istest
def check_I_can_withdraw_money_on_my_account():
	# Given
	account = account_builder_with_balance_equal_zero()
	previous_balance = account.get_balance()
	amount_to_withdraw = 100
	# When
	account.withdraw(amount_to_withdraw)
	# Then
	expected_balance = previous_balance - amount_to_withdraw
	assert account.get_balance() == expected_balance

@istest
@raises(NotPossibleWithDrawException)
def check_i_cant_withdraw_negative_amount():
	# Given
	account = account_builder_with_balance_equal_zero()
	amount_to_withdraw = -100
	# When
	account.withdraw(amount_to_withdraw)
	# Then
	# Check exception is raised ( see decorator )


@istest
@raises(NotPossibleDepositException)
def check_i_cant_deposit_negative_amount():
	# Given
	account = account_builder_with_balance_equal_zero()
	amount_to_deposit = -100
	# When
	account.deposit(amount_to_deposit)
	# Then
	# Check exception is raised ( see decorator )
	
@istest
@raises(NotPossibleWithDrawException)
def check_I_cant_withdraw_money_on_my_account_if_i_have_not_enough_money():
	# Given
	account = account_builder_with_balance_equal_zero()
	amount_to_withdraw = 300
	# When
	account.withdraw(amount_to_withdraw)
	# Then
	# Check exception is raised ( see decorator )

@istest
def check_account_with_one_deposit_has_an_history_with_one_element():
	# Given
	account = account_builder_with_balance_equal_zero()
	account.deposit(100)
	# When
	history = account.get_history()
	# Then
	assert history.get_number_of_transactions() == 1

@istest
def check_if_history_date_is_relevant():
	# Given
	history = History()
	expected_date = date.today
	# When
	history.add_transaction(300, expected_date)
	# Then
	actual_date = history.get_last_transaction().get_date()
	assert actual_date == expected_date

@istest
def check_history_with_one_withdraw_has_a_negative_amount():
	# Given
	account = account_builder_with_balance_equal_zero()
	# When
	account.withdraw(100)
	# Then
	withdraw_transaction = account.get_history().get_last_transaction()
	assert withdraw_transaction.get_amount() < 0

@istest
def check_if_my_transfer_reaches_its_destination():
	# Given
	bank = Bank("HSBC")
	transfering_account, receiving_account = two_accounts_builder_with_balance_equal_zero(bank)
	amount_to_transfer = 100
	# When
	bank.transfer(transfering_account,receiving_account, amount_to_transfer)
	# Then
	assert receiving_account.get_balance() == amount_to_transfer
	assert transfering_account.get_balance() == -amount_to_transfer

@istest
@raises(NotPossibleWithDrawException)
def check_deposit_is_not_done_when_transfer_fail():
	# Given
	bank = Bank("HSBC")
	transfering_account, receiving_account = two_accounts_builder_with_balance_equal_zero(bank)
	amount_to_transfer = 300
	# When
	bank.transfer(transfering_account,receiving_account, amount_to_transfer)
	# Then
	# Check exception is raised ( see decorator )

@istest
@raises(TransferBetweenTwoBanksException)
def check_that_the_two_accounts_belongs_to_the_same_bank():
	# Given
	emitting_account = account_builder_with_balance_equal_zero()
	receiving_account = account_builder_with_balance_equal_zero()
	bank = Bank("HSBC")
	# When
	bank.transfer(emitting_account,receiving_account,200)
	# Then
	# Check exception is raised ( see decorator )
	

@istest
@raises(AssertionError)
def check_it_is_not_possible_to_use_account_builder_with_a_non_accurate_parameter():
	# Given
	# When1
	account_builder_with_balance_equal_zero(1)
	# Then
	# Check exception is raised ( see decorator )

@istest
def check_if_a_planned_monthly_regular_transfer_happened():
	#Given
	bank, transferrer, receiver = test_regular_transfer(100, Regular_Transfer.MONTHLY)

	#When
	bank.operate_transfer(Regular_Transfer.MONTHLY)

	#Then
	assert(test_if_transfer_happened(transferrer, receiver))

@istest
def check_if_seven_planned_daily_regular_transfer_happened():
	#Given
	bank, transferrer, receiver = test_regular_transfer(100, Regular_Transfer.DAILY)

	#When
	bank.operate_daily_transfer(Regular_Transfer.DAILY)

	#Then
	assert(test_if_transfer_happened(transferrer, receiver))


def test_regular_transfer(amount, frequency):
	bank = Bank("HSBC")
	transferrer, receiver = two_accounts_builder_with_balance_equal_zero(bank)
	bank.create_regular_transfer(transferrer, receiver, amount, frequency)
	return bank, transferrer, receiver

def test_if_transfer_happened(transferrer, receiver):
	transferrer_transaction = transferrer.get_history().get_last_transaction()
	transferrer_sent_money = transferrer.get_amount() < 0
	receiver_transaction = receiver.get_history().get_last_transaction()
	receiver_received_money = receiver.get_amount() > 0
	return transferrer_sent_money && receiver_received_money
	
