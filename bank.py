#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import date
import uuid

def get_unique_id():
    return uuid.uuid4()

class Account():
    def __init__(self, id_account):
        self.balance = 0
        self.id = id_account
        self.history = History()

    def get_id(self):
        return self.id

    def get_history(self):
        return self.history

    def get_balance(self):
        return self.balance

    def deposit(self, amount , date = date.today):
        if not self.can_deposit(amount):
            raise NotPossibleDepositException(amount, self.balance)
        self.balance += amount
        self.history.add_transaction(amount, date)

    def can_deposit(self, amount):
        return amount > 0

    def withdraw(self, amount, date = date.today):
        if not self.can_withdraw(amount):
            raise NotPossibleWithDrawException(amount, self.balance)
        self.balance -= amount
        self.history.add_transaction(-amount, date)

    def can_withdraw(self, amount):
        return (self.balance - amount >= -200) and amount > 0


class HistoryTransaction():
    def __init__(self, amount, date):
        self.amount = amount
        self.date = date

    def get_date(self):
        return self.date

    def get_amount(self):
        return self.amount

class History():
    def __init__(self):
        self.transactions = []

    def get_number_of_transactions(self):
        return len(self.transactions)

    def add_transaction(self, amount, date):
        self.transactions.append(HistoryTransaction(amount, date))

    def get_last_transaction(self):
        return self.transactions[-1]


class Regular_Transfer():

    MONTHLY = 0
    DAILY = 1

    def __init__(self, transferer, receiver, amount, frequency):
        self.transferer = transferer
        self.receiver = receiver
        self.amount = amount
        self.frequency = frequency



class Bank():


    def __init__(self, nom):
        self.dico_of_accounts = {}
        self.nom = nom
        self.list_of_regular_transfers = []

    def create_account(self):
        id_account = get_unique_id()
        self.dico_of_accounts[id_account] = Account(id_account)
        return id_account

    def get_account(self, id_account):
        return self.dico_of_accounts[id_account]

    def account_exists(self, id_account):
        return id_account in self.dico_of_accounts

    def transfer(self, transferer, receiver, amount):
        if ( not self.account_exists(transferer.get_id()) or not self.account_exists(receiver.get_id())):
            raise TransferBetweenTwoBanksException(self.nom)
        transferer.withdraw(amount)
        receiver.deposit(amount)

    def create_regular_transfer(self, transferer, receiver, amount, frequency):
        self.list_of_regular_transfers.append(Regular_Transfer(transferer, receiver, amount, frequency))

    def operate_transfer(self, frequency):
        for regulate_transfer in self.list_of_regular_transfers:
            if(regulate_transfer.frequency==frequency):
                self.transfer(regulate_transfer.transferer, regulate_transfer.receiver, regulate_transfer.amount)



class NotPossibleWithDrawException(Exception):
    def __init__(self,amount, balance):
        self.amount = amount
        self.balance = balance
    
    def __str__(self):
        return "Impossible de retirer le montant demandé : {0}. Votre balance est de {1}".format(self.amount, self.balance)

class NotPossibleDepositException(Exception):
    def __init__(self,amount, balance):
        self.amount = amount
        self.balance = balance
    
    def __str__(self):
        return "Impossible de deposer le montant demandé : {0}. Votre balance est de {1}".format(self.amount, self.balance)

class TransferBetweenTwoBanksException(Exception):
    def __init__(self, bank):
        self.bank = bank

    def __str__(self):
        return "Les deux comptes n'appartiennent pas tous les deux à la banque {0}.".format(self.bank)

